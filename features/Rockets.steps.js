import { defineFeature, loadFeature } from 'jest-cucumber'

const feature = loadFeature('./features/Rockets.feature')

defineFeature(feature, test => {
  let rocket

  test('Launch a goHUNT Rocket', ({ given, when, then }) => {
    given("we're all billionaires", () => {
      rocket = {
        cost: 1000000000,
        location: 'launchpad',
        launch() {
          this.location = 'space'
        }
      }
    })

    when('I want to launch a Rocket', () => {
      rocket.launch()
    })

    then(/the rocket should fire into (\S+)/, location => {
      expect(rocket.location).toBe(location)
    })
  })
})
