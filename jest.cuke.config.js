module.exports = {
  preset: 'react-native',
  testPathIgnorePatterns: ['/node_modules/', '/lib/'],
  testRegex: '\\.steps\\.(ts|tsx|js)$',
  moduleFileExtensions: ['ts', 'tsx', 'js', 'json']
}
